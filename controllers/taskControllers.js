//Controllers contain the functions and business logic of our Express JS Application

const Task = require ("../models/Task");

//controller function for getting all the tasks

module.exports.getAllTasks = ()=>{
	return Task.find({}).then(result=>{
		return result;
	})
}

module.exports.createTask = (requestBody) =>{
	let newTask = new Task({
		name: requestBody.name
	})
	return newTask.save().then((task, error)=>{
		if(error){
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}


module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndRemove(taskId).then((removedTask,err)=>{
		if(err){
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, newContent) =>{
	return Task.findById(taskId).then((result, error)=>{
		if(error){
			console.log(err);
			return false;
		}
		result.name = newContent.name;
		return result.save().then((updatedTask,saveErr)=>{
			if (saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})

	})
}

//ACTIVITY SOLUTION

//Controller function for retrieving a specific task

module.exports.getTasks = (taskId)=>{
	return Task.findById(taskId).then((result,err)=>{
		if(err){
			console.log(err);
			return false;
		} else {
			return result;
		}
	})
}

//Controller function for changing the status of a task to complete

module.exports.updateTask = (taskId, newStatus) =>{
	return Task.findById(taskId).then((result, error)=>{
		if(error){
			console.log(err);
			return false;
		}
		result.status = newStatus.status;
		return result.save().then((updatedTask,saveErr)=>{
			if (saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})

	})
}
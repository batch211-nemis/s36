const express = require ("express")

const mongoose = require("mongoose");

const taskRoutes = require("./routes/taskRoutes")

const app = express();
const port = 3001;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://admin123:admin123@cluster0.0rrwrps.mongodb.net/s36?retryWrites=true&w=majority",
	{
		useNewUrlParser:true,
		useUnifiedTopology: true
	}
);	

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));

db.once("open", ()=>console.log("Connected to MongoDB!"))

app.use("/tasks", taskRoutes);

app.listen(port, () => console.log(`Server running at port ${port}`))
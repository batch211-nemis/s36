const express = require("express");

const router = express.Router();

const taskController = require("../controllers/taskControllers");

//Routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed

router.get("/", (req, res)=>{
	taskController.getAllTasks().then(resultFromController=>res.send(resultFromController));
})

router.post("/", (req, res)=>{
	taskController.createTask(req.body).then(resultFromController=>res.send(resultFromController));
})

router.delete("/:id", (req, res)=>{
	taskController.deleteTask(req.params.id).then(resultFromController=>res.send(resultFromController));
})

router.put("/:id", (req, res)=>{
	taskController.updateTask(req.params.id,req.body).then(resultFromController=>res.send(resultFromController));
})


//ACTIVITY SOLUTION

//Route for getting a specific task
router.get("/:id", (req, res)=>{
	taskController.getTasks(req.params.id).then(resultFromController=>res.send(resultFromController));
})


//Route for changing the status of a task to complete
router.put("/:id", (req, res)=>{
	taskController.updateTask(req.params.id,req.body).then(resultFromController=>res.send(resultFromController));
})



module.exports = router;